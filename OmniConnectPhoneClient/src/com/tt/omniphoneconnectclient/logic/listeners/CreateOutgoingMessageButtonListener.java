package com.tt.omniphoneconnectclient.logic.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.tt.omniphoneconnectclient.ui.frame.CreateOutgoingMessageFrame;
import com.tt.omniphoneconnectclient.ui.tabs.BluetoothSMSTab;

public class CreateOutgoingMessageButtonListener implements ActionListener {
	
	private BluetoothSMSTab messageTab;

	public CreateOutgoingMessageButtonListener(BluetoothSMSTab tab) {
		this.messageTab = tab;
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		new CreateOutgoingMessageFrame(messageTab);
	}

}
