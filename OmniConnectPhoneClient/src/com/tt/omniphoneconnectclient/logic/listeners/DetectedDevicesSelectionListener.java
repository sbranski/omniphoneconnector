package com.tt.omniphoneconnectclient.logic.listeners;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import com.tt.omniphoneconnectclient.ui.tabs.BluetoothDiscoveryTab;

public class DetectedDevicesSelectionListener implements ListSelectionListener {

	private BluetoothDiscoveryTab theTab;
	
	public DetectedDevicesSelectionListener(BluetoothDiscoveryTab tabToSet) {
		this.theTab = tabToSet;
	}
	
	@Override
	public void valueChanged(ListSelectionEvent arg0) {
		if (!theTab.getConnectButton().isEnabled()) {
			theTab.getConnectButton().setEnabled(true);
		}
	}

}
