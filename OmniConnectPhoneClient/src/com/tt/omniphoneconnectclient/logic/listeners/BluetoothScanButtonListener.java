package com.tt.omniphoneconnectclient.logic.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.bluetooth.BluetoothStateException;
import javax.bluetooth.DiscoveryAgent;
import javax.bluetooth.LocalDevice;

import com.tt.omniphoneconnectclient.ui.tabs.BluetoothDiscoveryTab;

public class BluetoothScanButtonListener implements ActionListener {
	private BluetoothDiscoveryTab discoverPanel;
	public BluetoothScanButtonListener(BluetoothDiscoveryTab thePanel) {
		discoverPanel = thePanel;
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
        LocalDevice localDevice;
        BluetoothDiscoveryListener listener = new BluetoothDiscoveryListener(discoverPanel);
		try {
			localDevice = LocalDevice.getLocalDevice();
			DiscoveryAgent agent = localDevice.getDiscoveryAgent();
			agent.startInquiry(DiscoveryAgent.GIAC, listener);
			synchronized (BluetoothDiscoveryTab.lockout) {
				BluetoothDiscoveryTab.lockout.wait();
			}
			this.discoverPanel.getDetectedDevices().setModel(listener.getDeviceModel());
			this.discoverPanel.getConnectButton().setEnabled(false);
		} catch (BluetoothStateException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
        
	}

}
