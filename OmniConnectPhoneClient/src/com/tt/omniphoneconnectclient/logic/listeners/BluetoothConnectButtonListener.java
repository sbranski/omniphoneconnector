package com.tt.omniphoneconnectclient.logic.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.bluetooth.LocalDevice;
import javax.bluetooth.RemoteDevice;
import javax.bluetooth.UUID;

import com.intel.bluetooth.BluetoothConsts;
import com.tt.omniphoneconnectclient.ui.MainFrame;
import com.tt.omniphoneconnectclient.ui.tabs.BluetoothDiscoveryTab;
import com.tt.omniphoneconnectclient.ui.tabs.BluetoothOBEXPushTab;
import com.tt.omniphoneconnectclient.ui.tabs.BluetoothPhoneCallTab;
import com.tt.omniphoneconnectclient.ui.tabs.BluetoothSMSTab;

public class BluetoothConnectButtonListener implements ActionListener {
	private BluetoothDiscoveryTab discoverPanel;
	
	public BluetoothConnectButtonListener (BluetoothDiscoveryTab thePanel) {
		discoverPanel = thePanel;
	}
	@Override
	public void actionPerformed(ActionEvent arg0) {
		String selectedDevice = this.discoverPanel.getDetectedDevices().getSelectedValue();
		selectedDevice = selectedDevice.substring(0, selectedDevice.indexOf("(")).trim();
		UUID[] searchUuidSet = new UUID[] { BluetoothConsts.RFCOMM_PROTOCOL_UUID };
	    int[] attrIDs =  new int[] {BluetoothConsts.AttributeIDServiceName};
	    RemoteDevice btDevice = MainFrame.detectedBluetoothDevices.get(selectedDevice);
	    BluetoothServicesDiscoveryListener listener = new BluetoothServicesDiscoveryListener(selectedDevice);
	    synchronized(BluetoothDiscoveryTab.lockout) {
        	try {
        		System.out.println("search services on " + btDevice.getBluetoothAddress() + " " + btDevice.getFriendlyName(false));
        		LocalDevice.getLocalDevice().getDiscoveryAgent().searchServices(attrIDs, searchUuidSet, btDevice, listener);
				BluetoothDiscoveryTab.lockout.wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return;
			}
        }
	    BluetoothSMSTab smsTab = new BluetoothSMSTab(MainFrame.urlForRFCOMMDetectedDevice.get(selectedDevice));
	    BluetoothPhoneCallTab callTab = new BluetoothPhoneCallTab(MainFrame.urlForRFCOMMDetectedDevice.get(selectedDevice), MainFrame.urlForAudioDetectedDevice.get(selectedDevice));
	    BluetoothOBEXPushTab fileTransferTab = new BluetoothOBEXPushTab(MainFrame.urlForOBEXDetectedDevice.get(selectedDevice));
	    BluetoothIncommingMessageListener messageListener = new BluetoothIncommingMessageListener(callTab, smsTab);
	    BluetoothIncommingOBEXTransferListener obexListener = new BluetoothIncommingOBEXTransferListener(fileTransferTab);
	    if (discoverPanel.getFatherPane().getTabCount() == 1) {
	    	discoverPanel.getFatherPane().addTab("SMS Messages", smsTab);
	    	discoverPanel.getFatherPane().addTab("Phone Calls", callTab);
	    	discoverPanel.getFatherPane().addTab("File Transfer (OBEX)", fileTransferTab);
	    } else if (discoverPanel.getFatherPane().getTabCount() > 1 && discoverPanel.getFatherPane().getTitleAt(1).equals("SMS Messages")) {
	    	discoverPanel.getFatherPane().remove(1);
	    	discoverPanel.getFatherPane().addTab("SMS Messages", smsTab);
	    } else if (discoverPanel.getFatherPane().getTabCount() > 1 && discoverPanel.getFatherPane().getTitleAt(2).equals("Phone Calls")) {
	    	discoverPanel.getFatherPane().remove(2);
	    	discoverPanel.getFatherPane().addTab("Phone Calls", callTab);
	    }
	    messageListener.startListener();
	    obexListener.startListener();
	}

}
