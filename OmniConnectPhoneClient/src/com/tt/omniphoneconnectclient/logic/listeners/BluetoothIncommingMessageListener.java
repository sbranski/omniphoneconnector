package com.tt.omniphoneconnectclient.logic.listeners;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

import javax.bluetooth.BluetoothStateException;
import javax.microedition.io.Connector;
import javax.microedition.io.StreamConnection;
import javax.microedition.io.StreamConnectionNotifier;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;

import com.intel.bluetooth.BluetoothConsts;
import com.tt.omniphoneconnectclient.logic.dto.AbstractDTO.MessageType;
import com.tt.omniphoneconnectclient.logic.dto.PhoneContact;
import com.tt.omniphoneconnectclient.logic.dto.SMSMessage;
import com.tt.omniphoneconnectclient.ui.tabs.BluetoothPhoneCallTab;
import com.tt.omniphoneconnectclient.ui.tabs.BluetoothSMSTab;

public class BluetoothIncommingMessageListener implements Runnable {

	private BluetoothSMSTab smsPanel;
	private BluetoothPhoneCallTab callPanel;
	private Thread callListenerThread = null;
	public static final String SERVER_NAME = "Message Sending Service";
	
	public BluetoothIncommingMessageListener(BluetoothPhoneCallTab callPanel, BluetoothSMSTab smsPanel) {
		this.callPanel = callPanel;
		this.smsPanel = smsPanel;
	}
	
	public void startListener() {
		callListenerThread = new Thread(this);
		callListenerThread.start();         
	}
	
	@Override
	public void run() {
		try {
			System.out.println("btspp://localhost:" + BluetoothConsts.SERIAL_PORT_UUID.toString() + ";name=" + SERVER_NAME);
			StreamConnectionNotifier serverConnection = (StreamConnectionNotifier) Connector.open("btspp://localhost:" + BluetoothConsts.SERIAL_PORT_UUID + ";name=" + SERVER_NAME);
			while (true) {
				StreamConnection conn = serverConnection.acceptAndOpen();
				InputStream is = conn.openInputStream();
				System.out.println("Received incomming connection");
				StringBuilder messageBuilder = new StringBuilder();
				InputStreamReader messageStreamReader = new InputStreamReader(is);
				BufferedReader messageReader = new BufferedReader(messageStreamReader);
				String readout = messageReader.readLine();
				while (readout != null) {
					messageBuilder.append(readout);
					readout = messageReader.readLine();
				}
				System.out.println("Incomming notification ----> " + messageBuilder.toString());
				if (messageBuilder.toString() != null && !messageBuilder.toString().isEmpty()) {
					StringTokenizer tokenize = new StringTokenizer(messageBuilder.toString(), ";");
					String messageId = tokenize.nextToken().replace("\"", "");
					if (messageId.equals(MessageType.SMS.getMessagetype()) || messageId.equals(MessageType.CONTACT.getMessagetype())) {
						processMessage (messageId, tokenize.nextToken().replace("\"", ""), tokenize.nextToken().replace("\"", ""), tokenize.nextToken().replace("\"", ""));
					}
				}
				messageReader.close();
				messageStreamReader.close();
				is.close();
			}
		} catch (BluetoothStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void processMessage(String messageId, String number, String content, String date) {
		if (messageId.equals(MessageType.SMS.getMessagetype())) {
			SMSMessage aMessage = new SMSMessage();
			aMessage.setPhoneNumber(number);
			aMessage.setMessageContent(content);
			aMessage.setTime(date);
			StringBuilder formattedSMS = new StringBuilder();
            formattedSMS.append("From: ").append(aMessage.getPhoneNumber()).append("\n");
            formattedSMS.append("Message: ").append(aMessage.getMessageContent()).append("\n");
            formattedSMS.append("Sent on: ").append(aMessage.getTime()).append("\n");
            JOptionPane.showMessageDialog(null, formattedSMS.toString(), "Incomming SMS Message", JOptionPane.PLAIN_MESSAGE);
            ((DefaultListModel<String>)smsPanel.getIncommingMessagesList().getModel()).addElement(number + " " + content + " " + date);
		} else if (messageId.equals(MessageType.CONTACT.getMessagetype())) {
			PhoneContact messagePassedFromMobile = new PhoneContact();
        	messagePassedFromMobile.setNumber(number);
        	messagePassedFromMobile.setName(content);
        	messagePassedFromMobile.setTimeStamp(date);
        	StringBuilder formattedContact = new StringBuilder();
            formattedContact.append("From: ").append(messagePassedFromMobile.getNumber()).append("\n");
            formattedContact.append("Contact name: ").append(messagePassedFromMobile.getName()).append("\n");
            formattedContact.append("Sent on: ").append(messagePassedFromMobile.getTimeStamp()).append("\n");
            JOptionPane.showMessageDialog(null, formattedContact.toString(), "Incomming Call", JOptionPane.PLAIN_MESSAGE);
            ((DefaultListModel<String>)callPanel.getIncommingCallList().getModel()).addElement(number + " " + content + " " + date);
		}
	}

}
