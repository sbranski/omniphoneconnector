package com.tt.omniphoneconnectclient.logic.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JFileChooser;

import com.tt.omniphoneconnectclient.logic.serviceclients.BluetoothOBEXFilePushClient;
import com.tt.omniphoneconnectclient.ui.tabs.BluetoothOBEXPushTab;

public class CreateOutgoingFileButtonListener implements ActionListener {

	private BluetoothOBEXPushTab tab;
	private JFileChooser chooser = new JFileChooser();
	
	public CreateOutgoingFileButtonListener(BluetoothOBEXPushTab pushTab) {
		this.tab = pushTab;
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		 int returnVal = chooser.showOpenDialog(tab);
		 if (returnVal == JFileChooser.APPROVE_OPTION) {
			 File file = chooser.getSelectedFile();
			 BluetoothOBEXFilePushClient server = new BluetoothOBEXFilePushClient(tab, tab.getUrl(), file);
			 server.startServer();
		 }
	}

}
