package com.tt.omniphoneconnectclient.logic.listeners;

import java.io.IOException;
import java.util.HashMap;

import javax.bluetooth.DeviceClass;
import javax.bluetooth.DiscoveryListener;
import javax.bluetooth.RemoteDevice;
import javax.bluetooth.ServiceRecord;
import javax.swing.DefaultListModel;

import com.tt.omniphoneconnectclient.ui.MainFrame;
import com.tt.omniphoneconnectclient.ui.tabs.BluetoothDiscoveryTab;

public class BluetoothDiscoveryListener implements DiscoveryListener {
	DefaultListModel<String> deviceModel;
	
	public BluetoothDiscoveryListener(BluetoothDiscoveryTab thePanel) {
		deviceModel = new DefaultListModel<String>();
		MainFrame.detectedBluetoothDevices = new HashMap<String, RemoteDevice>();
	}
	
	@Override
	public void deviceDiscovered(RemoteDevice arg0, DeviceClass arg1) {
		String device = arg0.getBluetoothAddress();
		try {
			device += " (" + arg0.getFriendlyName(false) +")";
		} catch (IOException ex) {
			device += " (No alias found)";
		}
		MainFrame.detectedBluetoothDevices.put(arg0.getBluetoothAddress(), arg0);
		deviceModel.addElement(device);
		System.out.println("Found device " + device);
	}

	@Override
	public void inquiryCompleted(int arg0) {
		synchronized(BluetoothDiscoveryTab.lockout) {  
			BluetoothDiscoveryTab.lockout.notify();
		} 
	}

	@Override
	public void serviceSearchCompleted(int arg0, int arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void servicesDiscovered(int arg0, ServiceRecord[] arg1) {
		// TODO Auto-generated method stub

	}

	public DefaultListModel<String> getDeviceModel () {
		return deviceModel;
	}
	
}
