package com.tt.omniphoneconnectclient.logic.listeners;

import java.util.List;

import javax.bluetooth.DataElement;
import javax.bluetooth.DeviceClass;
import javax.bluetooth.DiscoveryListener;
import javax.bluetooth.RemoteDevice;
import javax.bluetooth.ServiceRecord;
import javax.bluetooth.UUID;

import com.intel.bluetooth.BluetoothConsts;
import com.tt.omniphoneconnectclient.tools.BluetoothServicesUUID;
import com.tt.omniphoneconnectclient.tools.DatseqToUUIDConverter;
import com.tt.omniphoneconnectclient.ui.MainFrame;
import com.tt.omniphoneconnectclient.ui.tabs.BluetoothDiscoveryTab;

public class BluetoothServicesDiscoveryListener implements DiscoveryListener {

	private String bluetoothDeviceMac;
	
	public BluetoothServicesDiscoveryListener(String macId) {
		bluetoothDeviceMac = macId;
	}
	
	@Override
	public void deviceDiscovered(RemoteDevice arg0, DeviceClass arg1) {
		
	}

	@Override
	public void inquiryCompleted(int arg0) {
		
	}

	@Override
	public void serviceSearchCompleted(int arg0, int arg1) {
		 System.out.println("service search completed!");
         synchronized(BluetoothDiscoveryTab.lockout){
        	 BluetoothDiscoveryTab.lockout.notifyAll();
         }
	}

	@Override
	public void servicesDiscovered(int arg0, ServiceRecord[] servRecord) {
         for (int i = 0; i < servRecord.length; i++) {
             String url = servRecord[i].getConnectionURL(ServiceRecord.NOAUTHENTICATE_NOENCRYPT, false);
             if (url == null) {
                 continue;
             }
             DataElement elem = servRecord[i].getAttributeValue(BluetoothConsts.ServiceClassIDList);
             List<UUID> obtainedUUIDs = DatseqToUUIDConverter.convert(elem.toString());
             for (UUID uuidToCheck : obtainedUUIDs) {
            	 System.out.println("service " + servRecord[i].getAttributeValue(0x0100).getValue().toString() + " found " + url);
            	 if (uuidToCheck.equals(BluetoothServicesUUID.OBEXObjectPush.getUUID())) {
            		 MainFrame.urlForOBEXDetectedDevice.put(bluetoothDeviceMac, url);
            	 } else if (uuidToCheck.equals(BluetoothServicesUUID.HeadsetAudioGateway.getUUID()) || uuidToCheck.equals(BluetoothServicesUUID.GenericAudio.getUUID())) {
            		 MainFrame.urlForAudioDetectedDevice.put(bluetoothDeviceMac, url);
            	 } else if (uuidToCheck.equals(BluetoothServicesUUID.SerialPort.getUUID())) {
            		 MainFrame.urlForRFCOMMDetectedDevice.put(bluetoothDeviceMac, url);
            	 } else {
            		 MainFrame.urlForRFCOMMDetectedDevice.put(bluetoothDeviceMac, url);
            	 }
             }
          
         }
	}

}
