package com.tt.omniphoneconnectclient.logic.listeners;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.bluetooth.UUID;
import javax.microedition.io.Connector;
import javax.obex.HeaderSet;
import javax.obex.Operation;
import javax.obex.ResponseCodes;
import javax.obex.ServerRequestHandler;
import javax.obex.SessionNotifier;
import javax.swing.DefaultListModel;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import com.tt.omniphoneconnectclient.ui.frame.TransferProgressFrame;
import com.tt.omniphoneconnectclient.ui.tabs.BluetoothOBEXPushTab;

public class BluetoothIncommingOBEXTransferListener implements Runnable {
	
	private Thread callListenerThread = null;
	private BluetoothOBEXPushTab obexTab;
	private UUID obexUuid = new UUID(0x1105);

	private static class IncommingOBEXRequestHandler extends ServerRequestHandler {
		private BluetoothOBEXPushTab obexTab;
		public IncommingOBEXRequestHandler(BluetoothOBEXPushTab tab) {
			obexTab = tab;
		}
		 public int onPut(Operation operation) {
			 try {
	                HeaderSet hs = operation.getReceivedHeaders();
	                String name = (String) hs.getHeader(HeaderSet.NAME);
	                Long size = (Long) hs.getHeader(HeaderSet.LENGTH);
	                int selectedAnswer = JOptionPane.showConfirmDialog(obexTab, "Do You want to accept the \nincomming file " + name, "Accept incomming file", JOptionPane.YES_NO_OPTION);
	                if (selectedAnswer == JOptionPane.NO_OPTION) {
	                	return ResponseCodes.OBEX_HTTP_FORBIDDEN;
	                }
	                JFileChooser fileChooser = new JFileChooser();
	                File file = new File(System.getProperty("user.home") + "/" + name);
	                fileChooser.setSelectedFile(file);
	                int status = fileChooser.showSaveDialog(obexTab);
	                if (status == JFileChooser.APPROVE_OPTION) {
	                	InputStream incommingFileStream = operation.openInputStream();
	                	file = fileChooser.getSelectedFile();
	                	TransferProgressFrame frame = new TransferProgressFrame(size, name);
	                	OutputStream savedFileStream = new FileOutputStream(file);
	                	int eof;
	                	Long transferedSoFar = 0l;
	                	byte[] data = new byte[100];
	                	while ((eof = incommingFileStream.read(data)) != -1) {
	                		savedFileStream.write(data);
	                		transferedSoFar += 100l;
	                		frame.updateStatus(transferedSoFar);
	                	}
	                	savedFileStream.flush();
	                	savedFileStream.close();
	                	incommingFileStream.close();
	                	operation.close();
	                	frame.dispose();
	                	((DefaultListModel<String>)obexTab.getIncommingFileList().getModel()).addElement(name);
		                return ResponseCodes.OBEX_HTTP_OK;
	                }
	                return ResponseCodes.OBEX_HTTP_OK;
	            } catch (IOException e) {
	                e.printStackTrace();
	                return ResponseCodes.OBEX_HTTP_UNAVAILABLE;
	            }

		 }
	}

	public void startListener() {
		callListenerThread = new Thread(this);
		callListenerThread.start();         
	}
	
	public BluetoothIncommingOBEXTransferListener(BluetoothOBEXPushTab tab) {
		this.obexTab = tab;
	}
	
	@Override
	public void run() {
		try {
			SessionNotifier serverConnection = (SessionNotifier) Connector.open("btgoep://localhost:" + obexUuid + ";name=Incomming Obex File Transfer Server");
			while (true) {
				IncommingOBEXRequestHandler handler = new IncommingOBEXRequestHandler(obexTab);
	            serverConnection.acceptAndOpen(handler);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
}
