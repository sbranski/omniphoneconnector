package com.tt.omniphoneconnectclient.logic.serviceclients;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.microedition.io.Connector;
import javax.obex.ClientSession;
import javax.obex.HeaderSet;
import javax.obex.Operation;
import javax.obex.ResponseCodes;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;

import com.tt.omniphoneconnectclient.ui.frame.TransferProgressFrame;
import com.tt.omniphoneconnectclient.ui.tabs.BluetoothOBEXPushTab;

public class BluetoothOBEXFilePushClient implements Runnable {

	private String urlForObexPush;
	private File fileToSend;
	private BluetoothOBEXPushTab tab;
	private Thread obexServerThread = null;
	
	public BluetoothOBEXFilePushClient(BluetoothOBEXPushTab panel, String url, File file) {
		urlForObexPush = url;
		fileToSend = file;
		this.tab = panel;
	}
	
	public void startServer() {
		obexServerThread = new Thread(this);
		getSmsServerThread().start();          
	}
	
	public Thread getSmsServerThread() {
		return obexServerThread;
	}
	
	@Override
	public void run() {
		 ClientSession clientSession;
		try {
			clientSession = (ClientSession) Connector.open(urlForObexPush);
			HeaderSet hsConnectReply = clientSession.connect(null);
		    if (hsConnectReply.getResponseCode() != ResponseCodes.OBEX_HTTP_OK) {
		    	System.out.println("Failed to connect");
		        return;
		    }
		    HeaderSet hsOperation = clientSession.createHeaderSet();
	        hsOperation.setHeader(HeaderSet.NAME, fileToSend.getName());
	        Path path = Paths.get(fileToSend.getAbsolutePath());
	        String type = Files.probeContentType(path);
	        hsOperation.setHeader(HeaderSet.TYPE, type);
	        hsOperation.setHeader(HeaderSet.LENGTH, fileToSend.length());
	        Operation putOperation = clientSession.put(hsOperation);
	        RandomAccessFile file = new RandomAccessFile(fileToSend, "r");
	        TransferProgressFrame frame = new TransferProgressFrame(fileToSend.length(), fileToSend.getName());
	        OutputStream os = putOperation.openOutputStream();
            byte[] data = new byte[100];
            int eof;
            Long transferedSoFar = 0l;
            while ((eof = file.read(data)) != -1) {
            	os.write(data);
            	transferedSoFar += 100l;
        		frame.updateStatus(transferedSoFar);
            }
            os.close();
            putOperation.close();
            clientSession.disconnect(null);
            clientSession.close();
            frame.dispose();
            JOptionPane.showMessageDialog(null, "File " + fileToSend.getAbsolutePath() + " \n transfered to mobile phone!", "File sent to phone", JOptionPane.PLAIN_MESSAGE);
            ((DefaultListModel<String>)tab.getOutgoingFileList().getModel()).addElement(fileToSend.getAbsolutePath());
		} catch (IOException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "The file " + fileToSend.getAbsolutePath() + " \n could not be sent to mobile phone!", "Error while sending file to phone", JOptionPane.ERROR_MESSAGE);
			return;
		}
	}

}
