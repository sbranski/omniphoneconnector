package com.tt.omniphoneconnectclient.logic.serviceclients;

import java.io.IOException;
import java.io.OutputStream;

import javax.bluetooth.BluetoothStateException;
import javax.microedition.io.Connector;
import javax.microedition.io.StreamConnection;
import javax.swing.JPanel;

import com.tt.omniphoneconnectclient.logic.dto.AbstractDTO;
import com.tt.omniphoneconnectclient.logic.dto.SMSMessage;

public class BluetoothRFCOMMClientForSMS implements Runnable {
	private String urlForRfcommSMS;
	private SMSMessage messageToSend;
	private Thread smsServerThread = null;
	
	public BluetoothRFCOMMClientForSMS(JPanel panel, String url, SMSMessage message) {
		urlForRfcommSMS = url;
		messageToSend = message;
	}
	
	public void startServer() {
		smsServerThread = new Thread(this);
		getSmsServerThread().start();          
	}
	
	@Override
	public void run() {
		try {
			System.out.println("Establishing RFCOMM connection with " + urlForRfcommSMS);
			StreamConnection cs = (StreamConnection)Connector.open(urlForRfcommSMS);
			OutputStream phoneRfcommStream = cs.openOutputStream();
			StringBuilder buildSMS = new StringBuilder("\"").append(AbstractDTO.MessageType.SMS.getMessagetype()).append("\";\"").append(messageToSend.getPhoneNumber()).append("\";\"").append(messageToSend.getMessageContent()).append("\";\"").append(messageToSend.getTime()).append("\"");
			phoneRfcommStream.write(buildSMS.toString().getBytes());
			phoneRfcommStream.close();
			cs.close();
		} catch (BluetoothStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
	}

	public Thread getSmsServerThread() {
		return smsServerThread;
	}
}
