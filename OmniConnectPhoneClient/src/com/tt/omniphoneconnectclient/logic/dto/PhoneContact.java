package com.tt.omniphoneconnectclient.logic.dto;

public class PhoneContact extends AbstractDTO {

    private String number;
    private String name;
    private String timeStamp;

    public PhoneContact () {
    	
    }
    
    public PhoneContact (String number, String name, String timeStamp) {
        this.number = number;
        this.name = name;
        this.timeStamp = timeStamp;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }
}
