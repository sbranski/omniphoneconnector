package com.tt.omniphoneconnectclient.logic.dto;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class AbstractDTO {
	public enum MessageType {
		SMS("Sms"), CALL("Call"), CONTACT("Contact");
		private final String messagetype;
		
		public String getMessagetype() {
			return this.messagetype;
		}
		
		MessageType(String type) {
			this.messagetype = type;
		}
	}
	 private SimpleDateFormat sentDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	 private Calendar calendar = Calendar.getInstance();
	 protected Long timeStamp;
	 protected MessageType type;
}
