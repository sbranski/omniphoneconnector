package com.tt.omniphoneconnectclient.tools;

import java.util.ArrayList;
import java.util.List;

import javax.bluetooth.UUID;

public class DatseqToUUIDConverter {
	
	private static String DATSEQ_START = "DATSEQ {";
	private static String UUID_TAG = "UUID";
	
	public static List<UUID> convert (String toConvert) {
		List<UUID> uuids = new ArrayList<UUID>();
		if (toConvert.startsWith(DATSEQ_START)) {
			toConvert = toConvert.replace(DATSEQ_START, "").replace("}", "");
			if (toConvert.contains(UUID_TAG)) {
				toConvert = toConvert.replace(UUID_TAG, "");
				toConvert = toConvert.replace("\n", "").trim();
				String [] uuidStrings = toConvert.split(" ");
				for (String uuid : uuidStrings) {
					for (BluetoothServicesUUID knownUUID : BluetoothServicesUUID.values()) {
						if (knownUUID.getUUID().toString().equals(uuid.trim())) {
							uuids.add(knownUUID.getUUID());
						}
					}
				}
			}
		}
		return uuids;
	}
}
