package com.tt.omniphoneconnectclient.tools;

import javax.bluetooth.UUID;

public enum BluetoothServicesUUID {
	GeneralProfile(0x0001), SerialPort(0x1101), LANAccessUsingPPP(0x1102), DialupNetworking(0x1103), IrMCSync(0x1104),
	OBEXObjectPush(0x1105), OBEXFileTransfer(0x1106), Headset(0x1108), CordlessTelephony(0x1109),
	AudioSource(0x110A), AudioSink(0x110B), AdvancedAudioDistribution(0x110D), HeadsetAudioGateway(0x1112),
	SIM_Access(0x112D), PhonebookAccessPCE(0x112E), PhonebookAccessPSE(0x112F), PhonebookAccess(0x1130),
	GenericNetworking(0x1201), GenericFileTransfer(0x1202), GenericAudio(0x1203), GenericTelephony(0x1204);
	
	private final UUID uuidValue;
	BluetoothServicesUUID(int shortUUID) {
		this.uuidValue = new UUID(shortUUID);
	}
	
	public UUID getUUID() {
		return this.uuidValue;
	}
}
