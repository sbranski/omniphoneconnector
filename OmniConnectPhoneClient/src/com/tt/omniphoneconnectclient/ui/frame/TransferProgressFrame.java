package com.tt.omniphoneconnectclient.ui.frame;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.DefaultBoundedRangeModel;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.border.EmptyBorder;

public class TransferProgressFrame extends JDialog {
	private static final long serialVersionUID = 3343909918470915170L;
	private JLabel jLabel0, jLabel1, transferedLabel, fileNameLabel;
	private JPanel dummyPanel, dummyPanel2;
	private Long fileSize;
	private JProgressBar progressBar;
	private DefaultBoundedRangeModel model = new DefaultBoundedRangeModel();


	public TransferProgressFrame(Long fileSizeValue, String name) {
		super();
		this.fileSize = fileSizeValue;
		this.setSize(400, 120);
		this.setTitle("Transfering file");
		BoxLayout boxLayout = new BoxLayout(getContentPane(), BoxLayout.Y_AXIS);
	    setLayout(boxLayout);
		add(getDummyPanel2(name));
		add(getDummyPanel());
		add(getProgressBar());
		this.setResizable(false);
		this.setVisible(true);
	}
	
	private JLabel getTransferedLabel() {
		if (transferedLabel == null) {
			transferedLabel = new JLabel();
			transferedLabel.setText("");
		}
		return transferedLabel;
	}
	
	private JLabel getFilenameLabel(String filename) {
		if (fileNameLabel == null) {
			fileNameLabel = new JLabel();
			fileNameLabel.setText(filename);
		}
		return fileNameLabel;
	}
	
	private JLabel getJLabel0() {
		if (jLabel0 == null) {
			jLabel0 = new JLabel();
			jLabel0.setText("Total bytes transfered: ");
		}
		return jLabel0;
	}
	
	private JLabel getJLabel1() {
		if (jLabel1 == null) {
			jLabel1 = new JLabel();
			jLabel1.setText("File name: ");
		}
		return jLabel1;
	}
	
	private JPanel getDummyPanel() {
		if (dummyPanel == null) {
			dummyPanel = new JPanel();
			dummyPanel.add(getJLabel0());
			dummyPanel.add(getTransferedLabel());
		}
		return dummyPanel;
	}
	
	private JPanel getDummyPanel2(String filename) {
		if (dummyPanel2 == null) {
			dummyPanel2 = new JPanel();
			dummyPanel2.add(getJLabel1());
			dummyPanel2.add(getFilenameLabel(filename));
		}
		return dummyPanel2;
	}
	
	private JProgressBar getProgressBar() {
		if (progressBar == null) {
			progressBar = new JProgressBar();
			progressBar.setModel(model);
			model.setMinimum(0);
            model.setMaximum(100);
            EmptyBorder empty = (EmptyBorder) BorderFactory.createEmptyBorder(10, 10, 10, 10);
            progressBar.setBorder(empty);
		}
		return progressBar;
	}
	
	public void updateStatus(Long bytesTransferedSoFar) {
		transferedLabel.setText(bytesTransferedSoFar + "/" + fileSize);
		Double toSet = (double) (((double)bytesTransferedSoFar/(double)fileSize) * 100);
		model.setValue(safeLongToInt(toSet));
	}
	
	private static int safeLongToInt(double l) {
	    if (l < Integer.MIN_VALUE || l > Integer.MAX_VALUE) {
	        throw new IllegalArgumentException
	            (l + " cannot be cast to int without changing its value.");
	    }
	    return (int) l;
	}
}
