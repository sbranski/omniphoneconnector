package com.tt.omniphoneconnectclient.ui.frame;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import com.tt.omniphoneconnectclient.logic.dto.SMSMessage;
import com.tt.omniphoneconnectclient.logic.serviceclients.BluetoothRFCOMMClientForSMS;
import com.tt.omniphoneconnectclient.ui.tabs.BluetoothSMSTab;

public class CreateOutgoingMessageFrame extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;
	private BluetoothSMSTab messageTab;
	private JLabel jLabel0;
	private JTextField numberTextBox;
	private JLabel jLabel1;
	private JTextArea messageTextField;
	private JScrollPane jScrollPane0;
	private JButton sendButton;
	public CreateOutgoingMessageFrame(BluetoothSMSTab tab) {
		super();
		messageTab = tab;
		setTitle("New Message");
		initComponents();
	}
	
	public CreateOutgoingMessageFrame() {
		super();
		setTitle("New Message");
		initComponents();
	}

	private void initComponents() {
		setTitle("New Message");
		setResizable(false);
		setForeground(Color.black);
		setFont(new Font("Dialog", Font.PLAIN, 12));
		setLayout(new FlowLayout());
		add(getJLabel0());
		add(getNumberTextBox());
		add(getJLabel1());
		add(getJScrollPane0());
		add(getSendButton());
		setSize(414, 223);
		setVisible(true);
	}

	private JButton getSendButton() {
		if (sendButton == null) {
			sendButton = new JButton();
			sendButton.setText("Send");
			sendButton.addActionListener(this);
		}
		return sendButton;
	}

	private JScrollPane getJScrollPane0() {
		if (jScrollPane0 == null) {
			jScrollPane0 = new JScrollPane();
			jScrollPane0.setViewportView(getMessageTextField());
		}
		return jScrollPane0;
	}

	private JTextArea getMessageTextField() {
		if (messageTextField == null) {
			messageTextField = new JTextArea();
			messageTextField.setColumns(30);
			messageTextField.setLineWrap(true);
			messageTextField.setRows(8);
			messageTextField.setAutoscrolls(true);
		}
		return messageTextField;
	}

	private JLabel getJLabel1() {
		if (jLabel1 == null) {
			jLabel1 = new JLabel();
			jLabel1.setText("Message:");
		}
		return jLabel1;
	}

	private JTextField getNumberTextBox() {
		if (numberTextBox == null) {
			numberTextBox = new JTextField();
			numberTextBox.setPreferredSize(new Dimension(350, 20));
		}
		return numberTextBox;
	}

	private JLabel getJLabel0() {
		if (jLabel0 == null) {
			jLabel0 = new JLabel();
			jLabel0.setText("Number:");
		}
		return jLabel0;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		SMSMessage messageToSend = new SMSMessage();
		messageToSend.setMessageContent(this.getMessageTextField().getText());
		messageToSend.setPhoneNumber(this.getNumberTextBox().getText());
		messageToSend.setTime(Calendar.getInstance().getTime().toString());
		BluetoothRFCOMMClientForSMS server = new BluetoothRFCOMMClientForSMS(messageTab, messageTab.getUrl(), messageToSend);
		server.startServer();
		try {
			server.getSmsServerThread().join();
		} catch (InterruptedException e) {
			e.printStackTrace();
			return;
		}
		((DefaultListModel<String>)this.messageTab.getOutgoingMessagesList().getModel()).addElement(messageToSend.getMessageContent() + "; " + messageToSend.getPhoneNumber());
		dispose();
	}

}
