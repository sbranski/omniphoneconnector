package com.tt.omniphoneconnectclient.ui;

import java.util.HashMap;

import javax.bluetooth.RemoteDevice;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;



import com.tt.omniphoneconnectclient.ui.tabs.BluetoothDiscoveryTab;

public class MainFrame extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel connectPanel;
	private JTabbedPane mainTabbedPane;
	public static HashMap <String, RemoteDevice> detectedBluetoothDevices = new HashMap<String, RemoteDevice>();
	public static HashMap <String, String> urlForRFCOMMDetectedDevice = new HashMap<String, String>();
	public static HashMap <String, String> urlForOBEXDetectedDevice = new HashMap<String, String>();
	public static HashMap <String, String> urlForAudioDetectedDevice = new HashMap<String, String>();
	public MainFrame() {
		initComponents();
	}

	private void initComponents() {
		setTitle("Omni Phone Connect");
		add(getMainTabbedPane());
		setSize(800, 400);
	}

	private JTabbedPane getMainTabbedPane() {
		if (mainTabbedPane == null) {
			mainTabbedPane = new JTabbedPane();
			mainTabbedPane.addTab("Search for devices", getConnectPanel());
		}
		return mainTabbedPane;
	}

	private JPanel getConnectPanel() {
		if (connectPanel == null) {
			connectPanel = new BluetoothDiscoveryTab(getMainTabbedPane());
		}
		return connectPanel;
	}

	

	/**
	 * Main entry of the class.
	 * Note: This class is only created so that you can easily preview the result at runtime.
	 * It is not expected to be managed by the designer.
	 * You can modify it as you like.
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				MainFrame frame = new MainFrame();
				frame.setDefaultCloseOperation(MainFrame.EXIT_ON_CLOSE);
				frame.setTitle("Omni Phone Connect");
				frame.getContentPane().setPreferredSize(frame.getSize());
				frame.pack();
				frame.setResizable(false);
				frame.setLocationRelativeTo(null);
				frame.setVisible(true);
			}
		});
	}

}
