package com.tt.omniphoneconnectclient.ui.tabs;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.TitledBorder;

import com.tt.omniphoneconnectclient.logic.dto.SMSMessage;
import com.tt.omniphoneconnectclient.logic.listeners.CreateOutgoingMessageButtonListener;

public class BluetoothSMSTab extends JPanel {
	private static final long serialVersionUID = -5261772151335543213L;
	@SuppressWarnings("unused")
	private static ArrayList<SMSMessage> incomming = new ArrayList<SMSMessage>();
	@SuppressWarnings("unused")
	private static ArrayList<SMSMessage> outgoing = new ArrayList<SMSMessage>();
	private JPanel outgoingMessagesPanel;
	private String url;
	private JList<String> incommingMessagesList, outgoingMessagesList;
	private JScrollPane incommingMessagesScroll, outgoingMessagesScroll;
	private JButton newMessageButton;
	
	public BluetoothSMSTab(String urlAdd) {
		super();
		setUrl(urlAdd);
		initComponents();
	}
	
	public JList<String> getIncommingMessagesList() {
		if (incommingMessagesList == null) {
			incommingMessagesList = new JList<String>();
			DefaultListModel<String> listModel = new DefaultListModel<String>();
			incommingMessagesList.setModel(listModel);
		}
		return incommingMessagesList;
	}
	private JScrollPane getIncommingMessagesScroll() {
		if (incommingMessagesScroll == null) {
			incommingMessagesScroll = new JScrollPane();
			incommingMessagesScroll.setBorder(BorderFactory.createTitledBorder(null, "Incomming messages", TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION, new Font("Dialog",
					Font.BOLD, 12), new Color(51, 51, 51)));
			incommingMessagesScroll.setViewportView(getIncommingMessagesList());
		}
		return incommingMessagesScroll;
	}
	private void initComponents() {
		setLayout(new GridLayout(2, 3));
		add(getIncommingMessagesScroll());
		add(getOutgoingMessagesPanel());
		setSize(698, 240);
	}
	private JButton getNewMessageButton() {
		if (newMessageButton == null) {
			newMessageButton = new JButton();
			newMessageButton.setText("Create...");
			newMessageButton.addActionListener(new CreateOutgoingMessageButtonListener(this));
		}
		return newMessageButton;
	}
	private JScrollPane getOutgoingMessagesScroll() {
		if (outgoingMessagesScroll == null) {
			outgoingMessagesScroll = new JScrollPane();
			outgoingMessagesScroll.setPreferredSize(new Dimension(700, 165));
			outgoingMessagesScroll.setOpaque(false);
			outgoingMessagesScroll.setViewportView(getOutgoingMessagesList());
		}
		return outgoingMessagesScroll;
	}
	public JList<String> getOutgoingMessagesList() {
		if (outgoingMessagesList== null) {
			outgoingMessagesList = new JList<String>();
			DefaultListModel<String> listModel = new DefaultListModel<String>();
			outgoingMessagesList.setModel(listModel);
		}
		return outgoingMessagesList;
	}
	private JPanel getOutgoingMessagesPanel() {
		if (outgoingMessagesPanel == null) {
			outgoingMessagesPanel = new JPanel();
			outgoingMessagesPanel.setBorder(BorderFactory.createTitledBorder(null, "Outgoing messages", TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION, new Font("Dialog", Font.BOLD, 12), new Color(51, 51, 51)));
			outgoingMessagesPanel.add(getOutgoingMessagesScroll());
			outgoingMessagesPanel.add(getNewMessageButton());
		}
		return outgoingMessagesPanel;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
}
