package com.tt.omniphoneconnectclient.ui.tabs;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;

import com.tt.omniphoneconnectclient.logic.listeners.BluetoothConnectButtonListener;
import com.tt.omniphoneconnectclient.logic.listeners.BluetoothScanButtonListener;
import com.tt.omniphoneconnectclient.logic.listeners.DetectedDevicesSelectionListener;

public class BluetoothDiscoveryTab extends JPanel {
	private static final long serialVersionUID = -5586175175760464472L;
	private JScrollPane btScrollPane;
	private JButton connectButton = new JButton("Connect"), detectButton = new JButton("Detect Devices...");
	private JPanel buttonPanel = new JPanel();
	private JList<String> detectedDevices = new JList<String>();
	private BorderLayout layout;
	private FlowLayout buttonLayout;
	private JTabbedPane fatherPane;
	public static Object lockout = new Object();
	
	public BluetoothDiscoveryTab (JTabbedPane mainTab) {
		super();
		this.fatherPane = mainTab;
		this.setName("Discover Bluetooth Devices");
		detectedDevices.setModel(new DefaultListModel<String>());
		detectedDevices.addListSelectionListener(new DetectedDevicesSelectionListener(this));
		btScrollPane = new JScrollPane(detectedDevices);
		layout = new BorderLayout();
		buttonLayout = new FlowLayout();
		buttonPanel.setLayout(buttonLayout);
		buttonPanel.add(detectButton);
		buttonPanel.add(connectButton);
		connectButton.setEnabled(false);
		this.setLayout(layout);
		this.add(btScrollPane, BorderLayout.CENTER);
		this.add(buttonPanel, BorderLayout.PAGE_END);
		detectButton.addActionListener(new BluetoothScanButtonListener(this));
		connectButton.addActionListener(new BluetoothConnectButtonListener(this));
	}
	
	public JButton getConnectButton () {
		return this.connectButton;
	}
	
	public JButton getDetectButton () {
		return this.detectButton;
	}
	
	public void setDetectedDevices (JList<String> freshDevices) {
		this.detectedDevices = freshDevices;
	}
	
	public JList<String> getDetectedDevices () {
		return this.detectedDevices;
	}

	public JTabbedPane getFatherPane() {
		return fatherPane;
	}

}
