package com.tt.omniphoneconnectclient.ui.tabs;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.TitledBorder;

import com.tt.omniphoneconnectclient.logic.listeners.CreateOutgoingFileButtonListener;

public class BluetoothOBEXPushTab extends JPanel {
	private static final long serialVersionUID = -3575686178463513947L;
	private static ArrayList<String> incomming = new ArrayList<String>();
	private static ArrayList<String> outgoing = new ArrayList<String>();
	private String url;
	private JList<String> incommingFileList, outgoingFileList;
	private JScrollPane incommingFileScroll, outgoingFileScroll;
	private JPanel outgoingFilePanel;
	private JButton newFileTransferButton;
	
	public BluetoothOBEXPushTab(String urlAdd) {
		super();
		setUrl(urlAdd);
		initComponents();
	}
	
	private void initComponents() {
		setLayout(new GridLayout(2, 3));
		add(getIncommingFileScroll());
		add(getOutgoingFilesPanel());
		setSize(698, 240);
	}
	
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	private JScrollPane getIncommingFileScroll() {
		if (incommingFileScroll == null) {
			incommingFileScroll = new JScrollPane();
			incommingFileScroll.setBorder(BorderFactory.createTitledBorder(null, "Incomming files", TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION, new Font("Dialog",
					Font.BOLD, 12), new Color(51, 51, 51)));
			incommingFileScroll.setViewportView(getIncommingFileList());
		}
		return incommingFileScroll;
	}
	
	public JList<String> getIncommingFileList() {
		if (incommingFileList == null) {
			incommingFileList = new JList<String>();
			DefaultListModel<String> listModel = new DefaultListModel<String>();
			incommingFileList.setModel(listModel);
		}
		return incommingFileList;
	}

	private JScrollPane getOutgoingFileScroll() {
		if (outgoingFileScroll == null) {
			outgoingFileScroll = new JScrollPane();
			outgoingFileScroll.setPreferredSize(new Dimension(700, 165));
			outgoingFileScroll.setOpaque(false);
			outgoingFileScroll.setViewportView(getOutgoingFileList());
		}
		return outgoingFileScroll;
	}
	
	public JList<String> getOutgoingFileList() {
		if (outgoingFileList == null) {
			outgoingFileList = new JList<String>();
			DefaultListModel<String> listModel = new DefaultListModel<String>();
			outgoingFileList.setModel(listModel);
		}
		return outgoingFileList;
	}
	
	private JButton getNewFileTransferButton() {
		if (newFileTransferButton == null) {
			newFileTransferButton = new JButton();
			newFileTransferButton.setText("Send...");
			newFileTransferButton.addActionListener(new CreateOutgoingFileButtonListener(this));
		}
		return newFileTransferButton;
	}
	
	private JPanel getOutgoingFilesPanel() {
		if (outgoingFilePanel == null) {
			outgoingFilePanel = new JPanel();
			outgoingFilePanel.setBorder(BorderFactory.createTitledBorder(null, "Outgoing files", TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION, new Font("Dialog", Font.BOLD, 12), new Color(51, 51, 51)));
			outgoingFilePanel.add(getOutgoingFileScroll());
			outgoingFilePanel.add(getNewFileTransferButton());
		}
		return outgoingFilePanel;
	}
	
}
