package com.tt.omniphoneconnectclient.ui.tabs;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.TitledBorder;

import com.tt.omniphoneconnectclient.logic.dto.PhoneContact;

public class BluetoothPhoneCallTab extends JPanel {
	private static final long serialVersionUID = -5655399385365229011L;
	@SuppressWarnings("unused")
	private static ArrayList<PhoneContact> incomming = new ArrayList<PhoneContact>();
	private String url, urlForAudio;
	private JList<String> incommingCallList;
	private JScrollPane incommingCallsScroll;
	
	public BluetoothPhoneCallTab(String urlAdd, String audioUrl) {
		super();
		setUrl(urlAdd);
		setUrlForAudio(audioUrl);
		initComponents();
	}
	
	private JScrollPane getIncommingMessagesScroll() {
		if (incommingCallsScroll == null) {
			incommingCallsScroll = new JScrollPane();
			incommingCallsScroll.setBorder(BorderFactory.createTitledBorder(null, "Incomming calls", TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION, new Font("Dialog",
					Font.BOLD, 12), new Color(51, 51, 51)));
			incommingCallsScroll.setViewportView(getIncommingCallList());
		}
		return incommingCallsScroll;
	}
	
	public JList<String> getIncommingCallList() {
		if (incommingCallList == null) {
			incommingCallList = new JList<String>();
			DefaultListModel<String> listModel = new DefaultListModel<String>();
			incommingCallList.setModel(listModel);
		}
		return incommingCallList;
	}
	
	private void initComponents() {
		setLayout(new GridLayout(2, 3));
		add(getIncommingMessagesScroll());
		setSize(698, 240);
	}
	
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	public String getUrlForAudio() {
		return urlForAudio;
	}

	public void setUrlForAudio(String url) {
		this.urlForAudio = url;
	}
	
}
