package com.tt.codersday.omniconnectphone.logic;

import java.util.Calendar;

import com.tt.codersday.omniconnectphone.OmniPhoneMainWindow;
import com.tt.codersday.omniconnectphone.model.PhoneContact;

import android.bluetooth.BluetoothSocket;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract.PhoneLookup;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;

public class MobilePhoneStateListener extends PhoneStateListener {
	@Override
	public void onCallStateChanged(int state, String incomingNumber) {
		if (state == TelephonyManager.CALL_STATE_RINGING) {
			  BluetoothSocket socket = BluetoothHandler.connectForPassingIncommingNotifications();
			  PhoneContact phoneContact = new PhoneContact(incomingNumber, getContactName(OmniPhoneMainWindow.window.getApplicationContext(), incomingNumber), Calendar.getInstance().getTimeInMillis());
              BluetoothHandler.sendMessageToServer(socket, phoneContact);
		}
	}
	
	private static String getContactName(Context context, String phoneNumber) {
	    ContentResolver cr = context.getContentResolver();
	    Uri uri = Uri.withAppendedPath(PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));
	    Cursor cursor = cr.query(uri, new String[]{PhoneLookup.DISPLAY_NAME}, null, null, null);
	    if (cursor == null) {
	        return " ";
	    }
	    String contactName = " ";
	    if(cursor.moveToFirst()) {
	        contactName = cursor.getString(cursor.getColumnIndex(PhoneLookup.DISPLAY_NAME));
	    } if(cursor != null && !cursor.isClosed()) {
	        cursor.close();
	    }  return contactName;
	}
	
}
