package com.tt.codersday.omniconnectphone.logic;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;

import com.tt.codersday.omniconnectphone.model.AbstractDTO;
import com.tt.codersday.omniconnectphone.model.PhoneContact;
import com.tt.codersday.omniconnectphone.model.SMSMessage;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Set;
import java.util.UUID;

/**
 * Created by wykowskip on 2014-12-09.
 */
public class BluetoothHandler {

    private static Set<BluetoothDevice> pairedDevices;
    private static UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private static UUID uuid2 = UUID.fromString("00000001-0000-1000-8000-00805F9B34FB");

    public static String[] COLUMNS = new String[] {"address", "body", "date_sent"};

    private static final BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

    public static boolean isBluetoothAvailable() {
        return mBluetoothAdapter != null;
    }

    public static BluetoothSocket connectForPassingIncommingNotifications() {
        return connectForPassingIncommingNotifications(mBluetoothAdapter.getBondedDevices());
    }
    
    public static BluetoothSocket connectForPassingIncommingNotifications(Set<BluetoothDevice> devices) {
        pairedDevices = devices;
        for (BluetoothDevice bluetoothDevice : pairedDevices) {
            try {
                	BluetoothSocket socket = bluetoothDevice.createRfcommSocketToServiceRecord(uuid);
                	socket.connect();
                    if (socket != null && socket.isConnected()) {
                        return socket;
                    }
                    return socket;
            } catch (IOException e) {
                e.printStackTrace();
                BluetoothSocket socket;
				try {
					socket = bluetoothDevice.createInsecureRfcommSocketToServiceRecord(uuid);
					socket.connect();
	                if (socket != null && socket.isConnected()) {
	                    return socket;
	                }
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
					return null;
				}
                return null;
            }
        }
        return null;
    }

    public static BluetoothServerSocket createMobilePhoneSMSServerSocket() throws Exception {
	       BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
	       BluetoothServerSocket smsServerSocket = adapter.listenUsingInsecureRfcommWithServiceRecord("Message Sending Service", uuid2);
	       return smsServerSocket;
    }

    public static void sendMessageToServer(BluetoothSocket socket, AbstractDTO message) {
        DataOutputStream dos = null;
        if (socket != null) {
	        try {
	            dos = new DataOutputStream(socket.getOutputStream());
	            if (message instanceof PhoneContact) {
	            	dos.write(((PhoneContact)message).toString().getBytes());
	            } else if (message instanceof SMSMessage) {
	            	dos.write(((SMSMessage)message).toString().getBytes());
	            }
	            dos.flush();
	            socket.close();
	        } catch (IOException e) {
	            e.printStackTrace();
	            return;
	        }
        } else {
        	return;
        }
    }
   
    public static String readMessageFromServer(BluetoothSocket socket) {
        StringBuilder sb = new StringBuilder();
        if (socket != null) {
	        try {
	            byte[] buffer = new byte[1024];  // buffer store for the stream
	            int bytes = 0; // bytes returned from read()
	            InputStream is = socket.getInputStream();
	            // Keep listening to the InputStream until an exception occurs
	            while (bytes != -1) {
	                try {
	                    // Read from the InputStream
	                    bytes = is.read(buffer);
	                    sb.append(new String(buffer));
	                } catch (IOException e) {
	                    break;
	                }
	            }
	            socket.close();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
        }
        return sb.toString();
    }
}
