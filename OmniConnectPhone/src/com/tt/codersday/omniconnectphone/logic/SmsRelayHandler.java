package com.tt.codersday.omniconnectphone.logic;

import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.widget.Toast;

import com.tt.codersday.omniconnectphone.model.SMSMessage;

public class SmsRelayHandler extends BroadcastReceiver {

    @Override
	public void onReceive(Context context, Intent intent) {
		// Retrieves a map of extended data from the intent.
		final Bundle bundle = intent.getExtras();
		try {
			if (bundle != null) {
				final Object[] pdusObj = (Object[]) bundle.get("pdus");
				for (int i = 0; i < pdusObj.length; i++) {
					SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
					String phoneNumber = currentMessage.getDisplayOriginatingAddress();
					String message = currentMessage.getDisplayMessageBody();
                    if (!BluetoothHandler.isBluetoothAvailable()) {
                        int duration = Toast.LENGTH_LONG;
    					Toast toast = Toast.makeText(context, "Bluetooth adapter is not available!", duration);
    					toast.show();
                        return;
                    }
                    BluetoothSocket socket = BluetoothHandler.connectForPassingIncommingNotifications();
                    SMSMessage smsMessage = new SMSMessage(phoneNumber, message, currentMessage.getTimestampMillis());
                    BluetoothHandler.sendMessageToServer(socket, smsMessage);
				}
			}
		} catch (Exception ex) {
            ex.printStackTrace();
            return;
		}

	}

}
