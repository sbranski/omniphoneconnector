package com.tt.codersday.omniconnectphone.logic;

import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.telephony.SmsManager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class OutgoingSMSHandler extends Thread {
    private BluetoothServerSocket serverSocket = null;
    
    public OutgoingSMSHandler() throws Exception {
    	super();
    	serverSocket = BluetoothHandler.createMobilePhoneSMSServerSocket();
    }
    
    public void run() {
    	while (true) {
    		try {
				BluetoothSocket socket = serverSocket.accept();
				String message = BluetoothHandler.readMessageFromServer(socket);
				socket.close();
				System.out.println("SMS obtained is ----->" + message);
				StringTokenizer smsTokenizer = new StringTokenizer(message, ";");
				String number = "";
				String messageText = "";
				if (smsTokenizer.countTokens() == 4) {
					for (int index = 0; index < 4; index++) {
						String temp = smsTokenizer.nextToken().replace("\"", "");
						if (index == 1) {
							number = temp;
						} if (index == 2) {
							messageText = temp;
						}
					}
					SmsManager smsManager = SmsManager.getDefault();
					ArrayList<String> splitMessage = smsManager.divideMessage(messageText);
					smsManager.sendMultipartTextMessage(number, null, splitMessage, null, null);
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return;
			}
    	}
    }
   
}
