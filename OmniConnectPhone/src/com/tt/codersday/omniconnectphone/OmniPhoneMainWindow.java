package com.tt.codersday.omniconnectphone;

import java.io.FileInputStream;

import android.app.Activity;
import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;

import com.tt.codersday.omniconnectphone.logic.OutgoingSMSHandler;

public class OmniPhoneMainWindow extends Activity {

	private static AudioManager record = null;
	private static MediaPlayer play = null;
	public static OmniPhoneMainWindow window;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		window = this;
		setContentView(R.layout.activity_omni_phone_main_window);
		try {
			OutgoingSMSHandler handler = new OutgoingSMSHandler();
			handler.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public void startRecording(View view) {
	    if (record == null) {
	    	record = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
	    }
	    record.setBluetoothScoOn(true);
    	record.startBluetoothSco();
    	record.setMode(AudioManager.MODE_IN_CALL);
    	play = new MediaPlayer();
    	try {
			play.setDataSource(new FileInputStream("/sdcard/bluetooth/joyride.mp3").getFD());
			play.setAudioStreamType(AudioManager.STREAM_MUSIC);
			play.setVolume(75f, 75f);
	    	play.prepare();
	    	play.start();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	 }
	
	public void stopRecording(View view)  {
		play.stop();
		record.setBluetoothScoOn(false);
		record.stopBluetoothSco();
		record.setMode(AudioManager.MODE_NORMAL);
	}

}
