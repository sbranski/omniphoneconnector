package com.tt.codersday.omniconnectphone.model;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class AbstractDTO {
	public enum MessageType {
		SMS("Sms"), CALL("Call"), CONTACT("Contact");
		private final String messagetype;
		
		public String getMessagetype() {
			return this.messagetype;
		}
		
		MessageType(String type) {
			this.messagetype = type;
		}
	}
	 private SimpleDateFormat sentDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	 private Calendar calendar = Calendar.getInstance();
	 protected Long timeStamp;
	 protected MessageType type;
	 
	    protected void appendCSVData(StringBuilder sb, Object value, String columnName) {
	        sb.append("\"");
	        if (columnName.contains("date")) {
	            calendar.setTimeInMillis((Long) value);
	            sb.append(sentDateFormat.format(calendar.getTime()));
	        } else
	            sb.append(((String)value).replace(";", "."));
	        sb.append("\";");
	    }
}
