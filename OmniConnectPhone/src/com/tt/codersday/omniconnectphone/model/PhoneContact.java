package com.tt.codersday.omniconnectphone.model;

import com.tt.codersday.omniconnectphone.logic.BluetoothHandler;

public class PhoneContact extends AbstractDTO {

    private String number;
    private String name;

    public PhoneContact (String number, String name, long timeStamp) {
    	this.type = MessageType.CONTACT;
        this.number = number;
        this.name = name;
        this.timeStamp = timeStamp;
    }

    public String getNumber() {
        return number;
    }
    
    public MessageType getType () {
    	return this.type;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    @Override
    public String toString() {
        StringBuilder smsSB = new StringBuilder();
        appendCSVData(smsSB, this.type.getMessagetype(), "type");
        appendCSVData(smsSB, number, BluetoothHandler.COLUMNS[0]);
        appendCSVData(smsSB, name, BluetoothHandler.COLUMNS[1]);
        appendCSVData(smsSB, timeStamp, BluetoothHandler.COLUMNS[2]);
        smsSB.append("\n");
        return smsSB.substring(0, smsSB.length() - 1);
    }
}
