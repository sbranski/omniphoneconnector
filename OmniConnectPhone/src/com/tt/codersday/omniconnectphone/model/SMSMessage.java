package com.tt.codersday.omniconnectphone.model;

import com.tt.codersday.omniconnectphone.logic.BluetoothHandler;

/**
 * Created by wykowskip on 2014-12-10.
 */
public class SMSMessage extends AbstractDTO {

    private String address;
    private String message;

    public SMSMessage(String address, String message, long timeStamp) {
    	this.type = MessageType.SMS;
        this.address = address;
        this.message = message;
        this.timeStamp = timeStamp;
    }

    public String getAddress() {
        return address;
    }
    
    public MessageType getType () {
    	return this.type;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    @Override
    public String toString() {
        StringBuilder smsSB = new StringBuilder();
        appendCSVData(smsSB, this.type.getMessagetype(), "type");
        appendCSVData(smsSB, address, BluetoothHandler.COLUMNS[0]);
        appendCSVData(smsSB, message, BluetoothHandler.COLUMNS[1]);
        appendCSVData(smsSB, timeStamp, BluetoothHandler.COLUMNS[2]);
        smsSB.append("\n");
        return smsSB.substring(0, smsSB.length() - 1);
    }
}
